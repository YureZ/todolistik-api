﻿using Microsoft.EntityFrameworkCore;
using ToDoListikAPI.Model;

namespace ToDoListikAPI.Data
{
    public class DataContext : DbContext
    {
        //Конструктор
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }


        //Наборы данных
        public DbSet<ToDoList> ToDoLists { get; set; } = null!;

        public DbSet<ToDoItem> ToDoItems { get; set; } = null!;

        

    }
}
