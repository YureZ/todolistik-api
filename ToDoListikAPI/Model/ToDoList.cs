﻿using System.ComponentModel.DataAnnotations;

namespace ToDoListikAPI.Model
{
    public class ToDoList
    {
        public int Id { get; set; }

        [StringLength(100)]
        public string Name { get; set; } = string.Empty;

        public List<ToDoItem> ToDoItems { get; set; } = new List<ToDoItem>();
    }
}
