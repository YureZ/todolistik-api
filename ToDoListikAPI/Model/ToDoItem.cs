﻿using System.ComponentModel.DataAnnotations;

namespace ToDoListikAPI.Model
{
    public class ToDoItem
    {
        public int Id { get; set; }

        public bool Done { get; set; }

        [StringLength(100)]
        public string Name { get; set; } = string.Empty;


        //Внешний ключ (и так бы создавался в таблице, т.к. в ToDoList находится список ToDoItemов)
        public int ToDoListId { get; set; }
        //Навигационное свойство
        public ToDoList? ToDoList { get; set; }
    }
}
