﻿using AutoMapper;
using ToDoListikAPI.DTO;
using ToDoListikAPI.Model;

namespace ToDoListikAPI.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ToDoList, ToDoListDTO>().ReverseMap();
            CreateMap<ToDoItem, ToDoItemDTO>().ReverseMap();
        }
    }
}
