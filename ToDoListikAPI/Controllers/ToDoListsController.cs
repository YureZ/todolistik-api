﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ToDoListikAPI.Data;
using ToDoListikAPI.Model;
using ToDoListikAPI.DTO;
using AutoMapper;

namespace ToDoListikAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ToDoListsController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public ToDoListsController(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/ToDoLists
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ToDoListDTO>>> GetToDoLists()
        {
            //Берем из базы все списки дел
            List<ToDoList> toDoLists = await _context.ToDoLists.ToListAsync();
            //Мапим в исходный класс в DTO и возвращаем клиенту
            return _mapper.Map<List<ToDoListDTO>>(toDoLists);
        }

        // GET: api/ToDoLists/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ToDoListDTO>> GetToDoList(int id)
        {
            //Находим объект в БД по индексу
            var toDoList = await _context.ToDoLists.FindAsync(id);

            if (toDoList == null)
            {
                return NotFound();
            }

            //Если все ок - мапим его в DTO и возвращаем
            return _mapper.Map<ToDoListDTO>(toDoList);
        }

        // PUT: api/ToDoLists/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutToDoList(int id, ToDoListDTO toDoList)
        {
            if (id != toDoList.Id)
            {
                return BadRequest();
            }

            //Мапим входной DTO объект в объект для БД
            ToDoList doList = _mapper.Map<ToDoList>(toDoList);
            //Изменяем состояние на измененное для внесения изменений
            _context.Entry(doList).State = EntityState.Modified;

            try
            {
                //Вносим изменения если все ок
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ToDoListExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ToDoLists
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ToDoListDTO>> PostToDoList(ToDoListDTO toDoList)
        {
            //Мапим входной DTO объект в объект для БД
            ToDoList doList = _mapper.Map<ToDoList>(toDoList);
            //Добавляем новый объект в БД и сохраняем изменения
            _context.ToDoLists.Add(doList);
            await _context.SaveChangesAsync();
            //Если все ок - возвращаем полученный DTO объект назад в качестве подтверждения
            return CreatedAtAction("GetToDoList", new { id = toDoList.Id }, toDoList);
        }

        // DELETE: api/ToDoLists/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteToDoList(int id)
        {
            //Тут мапинг не нужен, возврата данных нет, а на вход только индекс объекта БД

            //Находим удяляемый объект в БД
            var toDoList = await _context.ToDoLists.FindAsync(id);
            if (toDoList == null)
            {
                return NotFound();
            }
            //Если нашли - удаляем и сохраняем изменения
            _context.ToDoLists.Remove(toDoList);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ToDoListExists(int id)
        {
            //Тут мапинг не нужен, возврат булевого значения, а на вход только индекс объекта БД
            return _context.ToDoLists.Any(e => e.Id == id);
        }
    }
}
