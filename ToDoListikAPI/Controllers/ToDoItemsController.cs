﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ToDoListikAPI.Data;
using ToDoListikAPI.DTO;
using ToDoListikAPI.Model;

namespace ToDoListikAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ToDoItemsController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public ToDoItemsController(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/ToDoItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ToDoItemDTO>>> GetToDoItems()
        {
            //Берем из базы все задачи списков дел
            List<ToDoItem> doItem = await _context.ToDoItems.Include(x => x.ToDoList).ToListAsync();
            //Мапим в исходный класс в DTO и возвращаем клиенту
            return _mapper.Map<List<ToDoItemDTO>>(doItem);
        }

        // GET: api/ToDoItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ToDoItemDTO>> GetToDoItem(int id)
        {
            //Находим объект в БД по индексу (включая родителя)
            var toDoItem = await _context.ToDoItems.Include(x => x.ToDoList).FirstOrDefaultAsync(y => y.Id.Equals(id));

            if (toDoItem == null)
            {
                return NotFound();
            }

            //Если все ок - мапим его в DTO и возвращаем
            return _mapper.Map<ToDoItemDTO>(toDoItem);
        }

        // PUT: api/ToDoItems/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutToDoItem(int id, ToDoItemDTO toDoItem)
        {
            if (id != toDoItem.Id)
            {
                return BadRequest();
            }

            //Мапим входной DTO объект в объект для БД
            ToDoItem doItem = _mapper.Map<ToDoItem>(toDoItem);
            //Находим родительский список задач по ID и помещаем в объект для правильного встраивания в контекст (или если он изменился)
            doItem.ToDoList = await _context.ToDoLists.FindAsync(doItem.ToDoList.Id);
            //Изменяем состояние на измененное для внесения изменений
            _context.Entry(doItem).State = EntityState.Modified;

            try
            {
                //Вносим изменения если все ок
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ToDoItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ToDoItems
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ToDoItemDTO>> PostToDoItem(ToDoItemDTO toDoItem)
        {
            //Мапим входной DTO объект в объект для БД
            ToDoItem doItem = _mapper.Map<ToDoItem>(toDoItem);

            //Находим родительский список задач по ID и помещаем в объект для правильного встраивания в контекст
            doItem.ToDoList = await _context.ToDoLists.FindAsync(doItem.ToDoList.Id);
            //Добавляем новый объект в БД и сохраняем изменения
            await _context.ToDoItems.AddAsync(doItem);
            await _context.SaveChangesAsync();

            //Если все ок - мапим обратно в DTO и возвращаем объект назад в качестве подтверждения
            toDoItem = _mapper.Map<ToDoItemDTO>(doItem);
            return CreatedAtAction("GetToDoItem", new { id = toDoItem.Id }, toDoItem);
        }

        // DELETE: api/ToDoItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteToDoItem(int id)
        {
            //Тут мапинг не нужен, возврата данных нет, а на вход только индекс объекта БД

            //Находим удяляемый объект в БД
            var toDoItem = await _context.ToDoItems.FindAsync(id);
            if (toDoItem == null)
            {
                return NotFound();
            }

            //Если нашли - удаляем и сохраняем изменения
            _context.ToDoItems.Remove(toDoItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ToDoItemExists(int id)
        {
            //Тут мапинг не нужен, возврат булевого значения, а на вход только индекс объекта БД
            return _context.ToDoItems.Any(e => e.Id == id);
        }
    }
}
