﻿namespace ToDoListikAPI.DTO
{
    public class ToDoItemDTO
    {
        public int Id { get; set; }

        public bool Done { get; set; }

        public string Name { get; set; } = string.Empty;


        //Родительский объект
        public ToDoListDTO? ToDoList { get; set; }
    }
}
